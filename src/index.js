import * as bootstrap from 'bootstrap';
import * as noUiSlider from 'nouislider';
import * as jQuery from 'jquery';
//import * as owlcarousel from 'owl.carousel';
//import { Tooltip, Toast, Popover } from 'bootstrap'
//console.log('index.js');

//double-range
var slider = document.getElementById('double-range');

noUiSlider.create(slider, {
    start: [20, 80],
    connect: true,
    range: {
        'min': 0,
        'max': 100
    },
});

//btn
$(document).ready(function () {
    var price //= $('.btn-price').text();
    $('.btn-price').hover(
        function () {
            price = $(this).text();
            $(this).text('В корзину');
        },
        function () {
            $(this).text(price);
        }

    );

    $('.btn-toggle-filter').click(function () {
        $('#catalogFilterContent').slideToggle("slow");
    })

    $(".new-productions_slider").owlCarousel({
        loop: true,
        autoplay: true,
        margin: 10,
        responsiveClass: false,
        items: 1,
        autoplayTimeout: 1000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            576: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    });
});


const webpack = require('webpack');
const path = require('path');
const PugPlugin = require('pug-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const sourceDirname = 'src/';

module.exports = {
  entry: {
    index: path.join(__dirname, 'src', 'index.pug'),
    //index: path.join(__dirname, 'src', 'index.js')
  },
  output: {
    path: path.join(__dirname, 'dist/'),
    publicPath: '/',
    filename: 'js/[name].[contenthash:8].js',
    //assetModuleFilename: 'asset/[name][ext]',
    clean: true,
  },
  resolve: {
    alias: {
      IMG: path.join(__dirname, './src/asset/img/'),
      FONTS: path.join(__dirname, './src/asset/fonts/'),
      ICONS: path.join(__dirname, './src/asset/icon/')
    }
  },
  module: {
    rules: [
      {
        test: /\.(woff2?|eot|ttf|otf|svg)$/i,
        include: /[\\/]fonts[\\/]/,
        type: 'asset/resource',
        generator: {
          //filename: 'assets/fonts/[name][ext][query]',
          filename: (pathData) => {
            console.log('FONT =>> ' + pathData.filename);
            const { dir } = path.parse(pathData.filename); // the filename is relative path by project
            const outputPath = dir.replace(sourceDirname, '');
            return outputPath + '/[name][ext]';
          },
        },
      },

      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },

      {
        test: /\.(css|scss|sass|less|styl)$/i,
        use: ['css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.pug$/i,
        loader: PugPlugin.loader,
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/i,
        include: /[\\/]img[\\/]/,
        type: 'asset/resource',
        generator: {
          //filename: path.join(__dirname, 'dist/', 'images/', '[name][hash:8][ext]'),
          //filename: 'assets/img/[name].[hash:8][ext]',
          filename: (pathData) => {
            console.log('IMG =>> ' + pathData.filename);
            const { dir } = path.parse(pathData.filename); // the filename is relative path by project
            const outputPath = dir.replace(sourceDirname, '');
            return outputPath + '/[name][ext]';
          },
        },
      },
      {
        test: /\.(ico|svg)$/i,
        include: /[\\/]icon[\\/]/,
        type: 'asset/resource',
        generator: {
          //filename: path.join('/images/icons', '[name].[contenthash:8][ext]'),
          filename: (pathData) => {
            console.log('ICON =>> ' + pathData.filename);
            const { dir } = path.parse(pathData.filename); // the filename is relative path by project
            const outputPath = dir.replace(sourceDirname, '');
            return outputPath + '/[name][ext]';
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new PugPlugin({
      extractComments: true,
      pretty: false, //!only development mode | build mode "pretty: false"
      verbose: true,
      extractCss: {
        filename: 'css/[name].[contenthash:8].css'
      },
      /*
      js: {
        filename: 'js/[name].[contenthash:8].js',
      },
      */
    }),
    new FileManagerPlugin({
      events: {
        onStart: {
          delete: ['dist'],
        },
        onEnd: {
          copy: [
            {
              source: path.join('src/asset', 'static'),
              destination: 'dist/asset/js/',
            },
          ],
        },
      },
    }),
    new WebpackNotifierPlugin(),
  ],
  target: 'web', //! run serve to watch live reload
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    watchFiles: {
      paths: ['src/**/*.*'],
      options: {
        usePolling: true,
      },
    },
    port: 9000,
    liveReload: true,
  },
  cache: false,
  optimization: {
    minimizer: [
      new ImageMinimizerPlugin({
        minimizer: {
          implementation: ImageMinimizerPlugin.imageminMinify,
          options: {
            plugins: [
              ['gifsicle', { interlaced: true }],
              ['jpegtran', { progressive: true }],
              ['optipng', { optimizationLevel: 5 }],
              ['svgo', { name: 'preset-default' }],
            ],
          },
        },
      }),
    ],
  },
}